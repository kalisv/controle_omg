<?php
function getOS () {
// Retourne le mdp de la BDD qui diffère selon Windows ou autres.
	if( strtolower(substr(PHP_OS, 0, 3)) === "win") {
		$os = "";
	}else {
		$os = "root";
	}
	return $os;
}


function connexion ($os) {
	// Se connecte à la BDD
	// $mdp = "" ou 'root'
	// $mdp est la valeur de retour de getOS()
	// Utiliser la valeur de retour comme variable de connexion pour les requêtes SQL

	try{
		$bdd = new PDO('mysql:host=localhost;dbname=devoirmars2017', 'root', $os);
	}catch (Exception $e){
		die('Erreur : ' . $e->getMessage());
	}
	$bdd->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
	return $bdd;
}
?>
