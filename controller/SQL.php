<?php

function checkEntry($entry, $length) {
    if (strlen($entry) > $length) {
        throw new ErrorException("Valeur de texte trop longue pour '".$libelle."'");
        exit();
    }
}

/***************************************************************************************************************************************************************
 * Partie ADD
 ***************************************************************************************************************************************************************/

function addUser(PDO $bdd, $pseudo, $email) {
    $query = "INSERT INTO `user` (pseudo, email) VALUES (?,?)";
    $prep = $bdd->prepare($query);
    $prep->execute(array($pseudo, $email));
    return $bdd->lastInsertId();
}

function addPage(PDO $bdd, $title, $chapitre, $book_id) {
    $query = "INSERT INTO `page` (title, chapitre, book_id) VALUES (?,?,?)";
    $prep = $bdd->prepare($query);
    $prep->execute(array($title, $chapitre, $book_id));
    return $bdd->lastInsertId();
}

function addBook(PDO $bdd, $title, $pic, $status, $user_id) {
    $query = "INSERT INTO `book` (title, pic, statut, user_id) VALUES (?,?,?,?)";
    $prep = $bdd->prepare($query);
    $prep->execute(array($title, $pic, $status, $user_id));
    return $bdd->lastInsertId();
}

function addDiv(PDO $bdd, $HTML, $page_id) {
    $query = "INSERT INTO `div` (HTML, page_id) VALUES (?,?)";
    $prep = $bdd->prepare($query);
    $prep->execute(array($HTML, $page_id));
    return $bdd->lastInsertId();
}

function addIngredient(PDO $bdd, $libelle) {
    checkEntry($libelle, 45);
    $query = "INSERT INTO `ingredients` (libelle) VALUES (?)";
    $prep = $bdd->prepare($query);
    $prep->execute(array($libelle));
    return $bdd->lastInsertId();
}

function addUnite(PDO $bdd, $libelle) {
    checkEntry($libelle, 45);
    $query = "INSERT INTO `unite` (libelle) VALUES (?)";
    $prep = $bdd->prepare($query);
    $prep->execute(array($libelle));
    return $bdd->lastInsertId();
}

function addRubrique(PDO $bdd, $libelle) {
    checkEntry($libelle);
    $query = "INSERT INTO `rubrique` (libelle) VALUES (?)";
    $prep = $bdd->prepare($query);
    $prep->execute(array($libelle));
    return $bdd->lastInsertId();
}

function addRecette(PDO $bdd, $nom, $descriptif, $niveauDiff, $tpsPrepa, $tpsCuisson, $nbPers, $image) {
    checkEntry($nom, 100);
    checkEntry($descriptif, 500);
    checkEntry($image, 200);
    $query = "  INSERT INTO `recette`
                (nom, description, niveauDiff, tpsPrepa, tpsCuisson, nbPers, image)
                VALUES (?,?,?,?,?,?,?)";
    $prep = $bdd->prepare($query);
    $prep->execute(array($nom, $descriptif, $niveauDiff, $tpsPrepa, $tpsCuisson, $nbPers, $image));
    return $bdd->lastInsertId();
}

function addComposer(PDO $bdd, $id_recette, $ingredient, $unite, $quantite) {
    $query = "  INSERT INTO `composer`
                (recette, ingredient, unite, quantite)
                VALUES (?,?,?,?)";
    $prep = $bdd->prepare($query);
    $prep->execute(array($id_recette, $ingredient, $unite, $quantite));
    return $bdd->lastInsertId();
}

function addAparternir(PDO $bdd, $id_rubrique, $id_recette) {
    $query = "  INSERT INTO `rubrique`
                (rubrique, recette)
                VALUES (?,?)";
    $prep = $bdd->prepare($query);
    $prep->execute(array($id_rubrique, $id_recette));
    return $bdd->lastInsertId();
}

/***************************************************************************************************************************************************************
* Partie get
***************************************************************************************************************************************************************/

function getRecetteHasDivFromRecette(PDO $bdd, $id_recette) {
    $query = "  SELECT rd.id
                FROM `recette` r
                JOIN `recette_has_div` rd
                ON r.id = rd.recette_id
                WHERE r.id = ?";
    $prep = $bdd->prepare($query);
    $prep->execute(array($id_recette));
    return $prep->fetchAll(PDO::FETCH_ASSOC);
}

function getRecetteHasDivFromDiv(PDO $bdd, $id_div) {
    $query = "  SELECT rd.id
                FROM `div` d
                JOIN `recette_has_div` rd
                ON d.id = rd.recette_id
                WHERE d.id = ?";
    $prep = $bdd->prepare($query);
    $prep->execute(array($id_div));
    return $prep->fetchAll(PDO::FETCH_ASSOC);
}

function authUser(PDO $bdd, $pseudo, $email) {//pour verifier la connexion
    $query = "SELECT * FROM `user` WHERE pseudo = ? AND email = ?";
    $prep = $bdd->prepare($query);
    $prep->execute(array($pseudo, $email));
    return $prep->fetchAll(PDO::FETCH_ASSOC)[0];
}

function getUser(PDO $bdd, $id) {//recuperer n'importe quel user
    $query = "SELECT * FROM `user` WHERE id = ?";
    $prep = $bdd->prepare($query);
    $prep->execute(array($id));
    return $prep->fetchAll(PDO::FETCH_ASSOC)[0];
}

function getUserBooks(PDO $bdd, $user_id) {
    $query = "  SELECT b.id
                FROM user u
                JOIN book b
                ON u.id = b.user_id
                WHERE u.id = ?";
    $prep = $bdd->prepare($query);
    $prep->execute(array($user_id));
    return $prep->fetchAll(PDO::FETCH_ASSOC);
}

function getBook(PDO $bdd, $id) {
    $query = "  SELECT id
                FROM book
                WHERE id = ?";
    $prep = $bdd->prepare($query);
    $prep->execute(array($id));
    return $prep->fetchAll(PDO::FETCH_ASSOC)[0];
}

function getBookPages(PDO $bdd, $book_id) { //retourne l'id des pages de book
    $query = "  SELECT p.id
                FROM book b
                JOIN page p
                ON b.id = p.book_id
                WHERE b.id = ?";
    $prep = $bdd->prepare($query);
    $prep->execute(array($book_id));
    return $prep->fetchAll(PDO::FETCH_ASSOC);
}

function getPage(PDO $bdd, $id) {
    $query = "  SELECT id
                FROM page
                WHERE id = ?";
    $prep = $bdd->prepare($query);
    $prep->execute(array($id));
    return $prep->fetchAll(PDO::FETCH_ASSOC)[0];
}

function getPageDivs(PDO $bdd, $page_id) {
    $query = "  SELECT *
                FROM page p
                JOIN div d
                ON p.id = d.page_id
                WHERE p.id = ?";
    $prep = $bdd->prepare($query);
    $prep->execute(array($page_id));
    return $prep->fetchAll(PDO::FETCH_ASSOC);
}

function getDiv(PDO $bdd, $id) {
    $query = "  SELECT id
                FROM div
                WHERE id = ?";
    $prep = $bdd->prepare($query);
    $prep->execute(array($id));
    return $prep->fetchAll(PDO::FETCH_ASSOC)[0];
}

/***************************************************************************************************************************************************************
* Partie modif
***************************************************************************************************************************************************************/

function modifBook(PDO $bdd, $title, $pic, $status, $user_id, $book_id) {
    //DELETE
    $bdd->query("SET foreign_key_checks = 0");
    removeBook($bdd, $book_id);
    //REINSERT
    $query = "INSERT INTO `user` (id, title, pic, statut, user_id) VALUES (?,?,?,?)";
    $prep = $bdd->prepare($query);
    $prep->execute(array($book_id, $title, $pic, $status, $user_id));
    $bdd->query("SET foreign_key_checks = 1");
    return $bdd->lastInsertId();
}

function modifPage(PDO $bdd, $title, $chapitre, $book_id, $page_id) {
    $bdd->query("SET foreign_key_checks = 0");
    removePage($bdd, $page_id);
    $query = "INSERT INTO `page` (id, title, chapitre, book_id) VALUES (?,?,?,?)";
    $prep = $bdd->prepare($query);
    $prep->execute(array($page_id, $title, $chapitre, $book_id));
    $bdd->query("SET foreign_key_checks = 1");
    return $bdd->lastInsertId();
}



function modifDiv(PDO $bdd, $HTML, $page_id, $div_id) {
    $bdd->query("SET foreign_key_checks = 0");
    removeDiv($bdd, $div_id);
    $query = "INSERT INTO `div` (id, HTML, page_id) VALUES (?,?,?)";
    $prep = $bdd->prepare($query);
    $prep->execute(array($div_id, $HTML, $page_id));
    $bdd->query("SET foreign_key_checks = 1");
    return $bdd->lastInsertId();
}

/***************************************************************************************************************************************************************
* Partie simple remove
***************************************************************************************************************************************************************/

function removeUser(PDO $bdd, $id) {
    $query = "DELETE FROM `user` WHERE id = ?";
    $prep = $bdd->prepare($query);
    $prep->execute(array($id));
}

function removeIngredient(PDO $bdd, $id) { //PAS clean --> inutile ?
    $query = "DELETE FROM `ingredient` WHERE id = ?";
    $prep = $bdd->prepare($query);
    $prep->execute(array($id));
}

function removeUnite(PDO $bdd, $id) { //PAS clean --> inutile ?
    $query = "DELETE FROM `unite` WHERE id = ?";
    $prep = $bdd->prepare($query);
    $prep->execute(array($id));
}
function removeComposer(PDO $bdd, $id) { //CLEAN
    $query = "DELETE FROM `composer` WHERE id = ?";
    $prep = $bdd->prepare($query);
    $prep->execute(array($id));
}
function removeRecette(PDO $bdd, $id) {//NOT CLEAN a une fonction clean
    $query = "DELETE FROM `recette` WHERE id = ?";
    $prep = $bdd->prepare($query);
    $prep->execute(array($id));
}
function removeRubrique(PDO $bdd, $id) {
    $query = "DELETE FROM `rubrique` WHERE id = ?";
    $prep = $bdd->prepare($query);
    $prep->execute(array($id));
}
function removeRecette_has_div(PDO $bdd, $id) { //CLEAN
    $query = "DELETE FROM `recette_has_div` WHERE id = ?";
    $prep = $bdd->prepare($query);
    $prep->execute(array($id));
}
function removeBook(PDO $bdd, $book_id) { //NOT CLEAN a une fonction clean
    $query = "DELETE FROM `book` WHERE id = ?";
    $prep = $bdd->prepare($query);
    $prep->execute(array($book_id));
}
function removePage(PDO $bdd, $page_id) {//NOT CLEAN a une fonction clean
    $query = "DELETE FROM `page` WHERE id = ?";
    $prep = $bdd->prepare($query);
    $prep->execute(array($page_id));
}
function removeDiv(PDO $bdd, $div_id) {//NOT CLEAN a une fonction clean
    $query = "DELETE FROM `div` WHERE id = ?";
    $prep = $bdd->prepare($query);
    $prep->execute(array($div_id));
}

function removeAppartenirRecette(PDO $bdd, $id_recette) { //CLEAN supprime par rapport a l'id de la recette
    $query = "DELETE FROM `appartenir` WHERE recette = ?";
    $prep = $bdd->prepare($query);
    $prep->execute(array($id_recette));
}
/**
 * CLEAN REMOVE
 */
function cleanRemoveRecette(PDO $bdd, $id_recette) {
    $RC = getRecetteHasDivFromRecette($bdd, $id_recette);
    foreach($RC as $id) {
        removeRecette_has_div($bdd, $id);
    }
    removeRecette($bdd, $id_recette);
}
function cleanRemoveDiv(PDO $bdd, $id_div) {
    $RC = getRecetteHasDivFromDiv($bdd, $id_div);
    foreach($RC as $id) {
        removeRecette_has_div($bdd, $id);
    }
    removeDiv($bdd, $id_div);
}

function cleanRemovePage(PDO $bdd, $page_id) {
    $divs = getPageDivs($bdd, $page_id);
    foreach($RC as $id) {
        cleanRemoveDiv($bdd, $id);
    }
    removePage($bdd, $page_id);
}

function cleanRemoveBook(PDO $bdd, $book_id) {
    $pages = getBookPages($bdd, $book_id);
    foreach($pages as $page_id) {
        cleanRemovePage($bdd, $page_id);
    }
    removeBook($bdd, $book_id);
}

function cleanRemoveUser(PDO $bdd, $user_id) {
    $books = getUserBooks($bdd, $user_id);
    foreach($books as $book_id) {
        $bdd->query("UPDATE `book` SET user_id = NULL");
    }
    removeUser($bdd, $user_id);
}
?>
