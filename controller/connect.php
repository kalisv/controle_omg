<?php
$dsn = "mysql:dbname=devoirMars2017; host=127.0.0.1; charset=utf8mb4";
$user = "root";
$password = "";
try {
    $bdd = new PDO($dsn, $user, $password);
} catch (PDOException $e) {
    $msg = [];
    $msg['error'] = "BDD error";
    echo JSON_encode($msg);
}
$bdd->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
?>
